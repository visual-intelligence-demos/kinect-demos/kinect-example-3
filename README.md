# Kinect Demo 3

This demo is an updated code-base from the kinect-example-2 provided files. You will find the updated code in [cv-demo.py](cv-demo.py).

This program has the following additional features:

- Three examples of detectors - use the `--detector` flag to specify which to use:
    + Harris Corners
    + Histogram of Oriented Gradients
    + Scale Invarient Feature Transform (SIFT)

For example: `python cv-demo.py --detector harris`

## Usage

The program is started in the same way as the initial demo.:

- `poetry install`
- `poetry shell`
- `python cv-demo.py`

It has the following arguments:

- `videofolder` provide it with a different video folder than the example video e.g. `python cv-demo.py D:\Set1`
- `--no-realtime` disables realtime checks and provides frames as fast as possible based on the rest of the code base.
- `--detector` specifies which detector to use:
   + `hog`
   + `harris`
   + `sift`
   + `none`
