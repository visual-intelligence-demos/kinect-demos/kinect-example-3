#!/usr/bin/env python

from argparse import ArgumentParser
from typing import Optional, Tuple
from FreenectPlaybackWrapper.PlaybackWrapper import FreenectPlaybackWrapper

import cv2
import numpy


def main():
    parser = ArgumentParser(description="OpenCV Demo for Kinect Coursework")
    parser.add_argument("videofolder", help="Folder containing Kinect video. Folder must contain INDEX.txt.",
                        default="ExampleVideo", nargs="?")
    parser.add_argument("--no-realtime", action="store_true", default=False)
    parser.add_argument("--detector", default="harris", choices=["harris", "none", "hog", "sift"])

    args = parser.parse_args()

    # Default parameters for threshold/offsets
    threshold = 90
    x_offset = -40
    y_offset = 20

    def on_threshold_change(value):
        """
        Handler to change threshold value

        :param value: Value to set the threshold to
        """
        nonlocal threshold

        threshold = value

    # Attach Trackbar to Thresholded Depth Window
    threshold_win_name = "Thresholded Depth"
    cv2.namedWindow(threshold_win_name)
    cv2.createTrackbar("Threshold", threshold_win_name, threshold, 255, on_threshold_change)

    def depth_click_handler(event, x, y, flags, param):
        """
        Mouse handler for Depth Image. Prints out the depth value when clicked.

        :param event: Mouse Event
        :param x: X co-ordinate
        :param y: Y co-ordinate
        :param flags: See OpenCV Docs
        :param param: Not Uses
        """
        nonlocal depth

        if event == cv2.EVENT_LBUTTONDOWN:
            print(f"Depth value: {depth[y][x]}")

    # Attach Mouse Callback for Depth Window
    cv2.namedWindow("Depth")
    cv2.setMouseCallback("Depth", depth_click_handler)

    bbox = None

    for status, rgb, depth in FreenectPlaybackWrapper(args.videofolder, not args.no_realtime):

        # If we have an updated Depth image, then start processing
        if status.updated_depth:

            # Threshold the image
            _, threshold_depth = cv2.threshold(depth, threshold, 255, cv2.THRESH_BINARY)

            # Get set of points where value in thresholded image is equal to 0
            # points in format (y, x)
            points = numpy.argwhere(threshold_depth == 0)

            if len(points) > 0:
                # Calculate Bounding Box around points
                bbox = cv2.boundingRect(points)

                # Extract out bounding box components
                (y, x, h, w) = bbox

                # Draw bounding box on depth images
                cv2.rectangle(threshold_depth, [int(x), int(y)], [int(x + w), int(y + h)], (0, 0, 255))
                cv2.rectangle(depth, [int(x), int(y)], [int(x + w), int(y + h)], (0, 0, 255))

            else:
                bbox = None

            # Show depth/thresholded depth images
            cv2.imshow("Depth", depth)
            cv2.imshow(threshold_win_name, threshold_depth)

        # If we have an updated rgb image, then draw
        if status.updated_rgb:

            # If we have a current bounding box, then draw that on the RGB image with offset
            if bbox is not None:
                # Extract out bounding box components
                (y, x, h, w) = bbox

                # Add x/y offsets for rgb image
                x = x + x_offset
                y = y + y_offset

                # Draw bounding box on RGB image
                cv2.rectangle(rgb, [int(x), int(y)], [int(x + w), int(y + h)], (0, 0, 255))

                if args.detector != "none":
                    if w > 5 and h > 5:
                        cropped_image = rgb[int(y + 1):int(y + h - 1), int(x + 1):int(x + w - 1), :]

                        if args.detector == "harris":
                            cropped_image = harris_corners(cropped_image)

                            rgb[int(y + 1):int(y + h - 1), int(x + 1):int(x + w - 1), :] = cropped_image

                        if args.detector == "hog":
                            hog_histogram(cropped_image)

                        if args.detector == "sift":
                            keypoint_image, _, _ = sift_keypoints(cropped_image)

                            rgb[int(y + 1):int(y + h - 1), int(x + 1):int(x + w - 1), :] = keypoint_image


            # Show RGB image
            cv2.imshow("RGB", rgb)

        # Check for Keyboard input.
        key = cv2.waitKey(5)

        # Break out of the program if ESC is pressed (OpenCV KeyCode 27 is ESC key)
        if key == 27:
            break

    return 0


def harris_corners(image: numpy.ndarray) -> numpy.ndarray:
    """
    Detects Harris Corners and returns an image of corners

    Based on: https://www.geeksforgeeks.org/feature-detection-and-matching-with-opencv-python/

    :param image: Image to process
    :return modified image showing Harris corners
    """

    # Convert the image to a floating point greyscale image
    grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    grey_image = numpy.float32(grey_image)

    # Apply harris corners
    corner_image = cv2.cornerHarris(grey_image, blockSize=2, ksize=3, k=0.04)

    # Dilate to mark image corners
    corner_image_dilate = cv2.dilate(corner_image, None)

    # Mark corners on image
    return_value = numpy.copy(image)
    return_value[corner_image_dilate > 0.01 * corner_image_dilate.max()] = [0, 255, 0]

    # Corner locations (unused in this demo)
    # points in format (y, x)
    points = numpy.argwhere(corner_image_dilate > 0.01 * corner_image_dilate.max())

    return return_value


def hog_histogram(image: numpy.ndarray, nbins: int = 9, display: bool = True) -> Optional[numpy.ndarray]:
    """
    Extracts a HOG descriptor from an image

    Based on: https://stackoverflow.com/a/37005437/791025

    :param image: Image to process
    :param display: Whether or not to display one of the bins (bin 5) of the HOG descriptor
    :return HOG Histogram if no errors, else None
    """

    # Convert the image to a greyscale image
    grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Create the HOG detector
    cell_size = (8, 8)
    block_size = (2, 2)

    hog_detector = cv2.HOGDescriptor(_winSize=(grey_image.shape[1] // cell_size[1] * cell_size[1],
                                               grey_image.shape[0] // cell_size[0] * cell_size[0]),
                                     _blockSize=(block_size[1] * cell_size[1],
                                                 block_size[0] * cell_size[0]),
                                     _blockStride=(cell_size[1], cell_size[0]),
                                     _cellSize=(cell_size[1], cell_size[0]),
                                     _nbins=nbins)

    try:
        # Compute the features
        hog_features_cv = hog_detector.compute(grey_image)
    except cv2.error:
        # Return None in case of error
        return None

    if display and len(hog_features_cv) > 0:

        n_cells = (grey_image.shape[0] // cell_size[0], grey_image.shape[1] // cell_size[1])
        hog_features = hog_features_cv.reshape(n_cells[1] - block_size[1] + 1,
                                               n_cells[0] - block_size[0] + 1,
                                               block_size[0], block_size[1], nbins).transpose((1, 0, 2, 3, 4))  # index blocks by rows first

        # hog_feats now contains the gradient amplitudes for each direction,
        # for each cell of its group for each group. Indexing is by rows then columns.

        gradients = numpy.zeros((n_cells[0], n_cells[1], nbins))

        # count cells (border cells appear less often across overlapping groups)
        cell_count = numpy.full((n_cells[0], n_cells[1], 1), 0, dtype=int)

        for off_y in range(block_size[0]):
            for off_x in range(block_size[1]):
                gradients[off_y:n_cells[0] - block_size[0] + off_y + 1,
                          off_x:n_cells[1] - block_size[1] + off_x + 1] += \
                    hog_features[:, :, off_y, off_x, :]
                cell_count[off_y:n_cells[0] - block_size[0] + off_y + 1,
                           off_x:n_cells[1] - block_size[1] + off_x + 1] += 1

        # Average gradients
        gradients /= cell_count

        show_image = gradients[:, :, 5]
        resized_gradients = cv2.resize(show_image, (show_image.shape[1] * 4, show_image.shape[0] * 4))

        cv2.imshow("HOG Descriptor", resized_gradients)

    return hog_features_cv


sift_detector = None


def sift_keypoints(image: numpy.ndarray) -> Tuple[numpy.ndarray, numpy.ndarray, numpy.ndarray]:
    """
    Detect SIFT keypoints, and draw the keypoints onto an image

    :param image: Image to process
    :return: Tuple[Keypoint Image, Keypoints, Descriptors]
    """
    global sift_detector

    # Convert the image to a greyscale image
    grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    if not sift_detector:
        sift_detector = cv2.SIFT_create()

    kp, des = sift_detector.detectAndCompute(grey_image, None)

    kp_image = cv2.drawKeypoints(image, kp, None, color=(0, 255, 0), flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    return kp_image, kp, des


if __name__ == "__main__":
    exit(main())
